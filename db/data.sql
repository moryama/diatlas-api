--
-- File generated with SQLiteStudio v3.2.1 on dom giu 21 11:43:37 2020
--
-- Text encoding used: UTF-8
--

BEGIN TRANSACTION;

-- Table: type
INSERT INTO type (
                     id,
                     name,
                     label,
                     label_plural
                 )
                 VALUES (
                     1,
                     'dialect',
                     'dialetto',
                     'dialetti'
                 );

INSERT INTO type (
                     id,
                     name,
                     label,
                     label_plural
                 )
                 VALUES (
                     2,
                     'voice',
                     'voce regionale',
                     'voci regionali'
                 );

-- Table: area
INSERT INTO area (
                     id,
                     name
                 )
                 VALUES (
                     1,
                     'Nord'
                 );

INSERT INTO area (
                     id,
                     name
                 )
                 VALUES (
                     2,
                     'Centro'
                 );

INSERT INTO area (
                     id,
                     name
                 )
                 VALUES (
                     3,
                     'Sud'
                 );

-- Table: region
INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       1,
                       'Abruzzo',
                       NULL,
                       2,
                       'L''Aquila'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       2,
                       'Basilicata',
                       NULL,
                       3,
                       'Potenza'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       3,
                       'Calabria',
                       NULL,
                       3,
                       'Catanzaro'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       4,
                       'Campania',
                       NULL,
                       3,
                       'Napoli'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       5,
                       'Emilia Romagna',
                       NULL,
                       1,
                       'Bologna'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       6,
                       'Friuli-Venezia-Giulia',
                       NULL,
                       1,
                       'Trieste'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       7,
                       'Lazio',
                       NULL,
                       2,
                       'Roma'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       8,
                       'Liguria',
                       NULL,
                       1,
                       'Genova'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       9,
                       'Lombardia',
                       NULL,
                       1,
                       'Milano'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       10,
                       'Marche',
                       NULL,
                       2,
                       'Ancona'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       11,
                       'Molise',
                       NULL,
                       3,
                       'Campobasso'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       12,
                       'Piemonte',
                       NULL,
                       1,
                       'Torino'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       13,
                       'Puglia',
                       NULL,
                       3,
                       'Bari'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       14,
                       'Sardegna',
                       NULL,
                       2,
                       'Cagliari'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       15,
                       'Sicilia',
                       NULL,
                       3,
                       'Palermo'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       16,
                       'Toscana',
                       NULL,
                       2,
                       'Firenze'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       17,
                       'Trentino-Alto-Adige',
                       NULL,
                       1,
                       'Trento'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       18,
                       'Umbria',
                       NULL,
                       2,
                       'Perugia'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       19,
                       'Valle d''Aosta',
                       NULL,
                       1,
                       'Aosta'
                   );

INSERT INTO region (
                       id,
                       name,
                       info,
                       area_id,
                       capital
                   )
                   VALUES (
                       20,
                       'Veneto',
                       NULL,
                       1,
                       'Venezia'
                   );
-- Table: province
INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         1,
                         'MI',
                         9
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         2,
                         'NA',
                         4
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         3,
                         'BO',
                         5
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         4,
                         'VE',
                         20
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         5,
                         'TA',
                         13
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         6,
                         'PI',
                         16
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         7,
                         'AQ',
                         1
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         8,
                         'PZ',
                         2
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         9,
                         'CS',
                         3
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         10,
                         'BA',
                         3
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         11,
                         'UD',
                         6
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         12,
                         'GO',
                         6
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         13,
                         'LT',
                         7
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         14,
                         'Roma',
                         7
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         15,
                         'GE',
                         8
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         16,
                         'AN',
                         10
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         17,
                         'MC',
                         10
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         18,
                         'IS',
                         11
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         19,
                         'CB',
                         11
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         20,
                         'CN',
                         12
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         21,
                         'TO',
                         12
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         22,
                         'CA',
                         14
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         23,
                         'NU',
                         14
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         24,
                         'ME',
                         15
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         25,
                         'PA',
                         15
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         26,
                         'PG',
                         18
                     );

INSERT INTO province (
                         id,
                         name,
                         region_id
                     )
                     VALUES (
                         27,
                         'Issime',
                         19
                     );

-- Table: city
INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     1,
                     'Milano',
                     1
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     2,
                     'Pisa',
                     6
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     3,
                     'Taranto',
                     5
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     4,
                     'Cazzago di Pianiga',
                     4
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     5,
                     'Bologna',
                     3
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     6,
                     'Napoli',
                     2
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     7,
                     'Venezia',
                     4
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     8,
                     'Pianiga',
                     4
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     9,
                     'L''Aquila',
                     7
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     10,
                     'Alianello',
                     8
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     11,
                     'Frascineto',
                     9
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     12,
                     'Noicattaro',
                     10
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     13,
                     'Agrons',
                     11
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     14,
                     'Gorizia',
                     12
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     15,
                     'Fondi',
                     13
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     16,
                     'Venere',
                     7
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     17,
                     'Roma',
                     14
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     18,
                     'Genova',
                     15
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     19,
                     'Jesi',
                     16
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     20,
                     'Tolentino',
                     17
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     21,
                     'Isernia',
                     18
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     22,
                     'Campobasso',
                     19
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     23,
                     'Bellino',
                     20
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     24,
                     'Torino',
                     21
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     25,
                     'Cagliari',
                     22
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     26,
                     'Nuoro',
                     23
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     27,
                     'San Fratello',
                     24
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     28,
                     'Palermo',
                     25
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     29,
                     'Castiglione del Lago',
                     26
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     30,
                     'Perugia',
                     26
                 );

INSERT INTO city (
                     id,
                     name,
                     province_id
                 )
                 VALUES (
                     31,
                     'Issime',
                     19
                 );


-- Table: langarea
INSERT INTO langarea (
                         id,
                         name
                     )
                     VALUES (
                         1,
                         'Gallo-italico'
                     );

INSERT INTO langarea (
                         id,
                         name
                     )
                     VALUES (
                         2,
                         'Meridionale intermedio'
                     );

INSERT INTO langarea (
                         id,
                         name
                     )
                     VALUES (
                         3,
                         'Veneto'
                     );

INSERT INTO langarea (
                         id,
                         name
                     )
                     VALUES (
                         4,
                         'Francoprovenzale'
                     );

INSERT INTO langarea (
                         id,
                         name
                     )
                     VALUES (
                         5,
                         'Toscano'
                     );

INSERT INTO langarea (
                         id,
                         name
                     )
                     VALUES (
                         6,
                         'Albanese'
                     );

INSERT INTO langarea (
                         id,
                         name
                     )
                     VALUES (
                         7,
                         'Friulano'
                     );

INSERT INTO langarea (
                         id,
                         name
                     )
                     VALUES (
                         8,
                         'Mediano'
                     );

INSERT INTO langarea (
                         id,
                         name
                     )
                     VALUES (
                         9,
                         'Occitano'
                     );

INSERT INTO langarea (
                         id,
                         name
                     )
                     VALUES (
                         10,
                         'Sardo'
                     );

INSERT INTO langarea (
                         id,
                         name
                     )
                     VALUES (
                         11,
                         'Walser'
                     );


-- Table: langdivision
INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             1,
                             'Bolognese',
                             1
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             2,
                             'Campano',
                             2
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             3,
                             'Veneto',
                             3
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             4,
                             'Tarantino',
                             2
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             5,
                             'Pisano',
                             5
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             6,
                             'Milanese',
                             4
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             7,
                             'Abruzzese',
                             2
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             8,
                             'Lucano',
                             2
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             9,
                             'Arbëresh',
                             6
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             10,
                             'Carnico',
                             7
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             11,
                             'Basso laziale',
                             8
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             12,
                             'Genovese',
                             1
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             13,
                             'Anconitano',
                             8
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             14,
                             'Molisano',
                             2
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             15,
                             'Piemontese',
                             9
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             16,
                             'Campidanese',
                             10
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             17,
                             'Siciliano orientale',
                             1
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             18,
                             'Umbro',
                             8
                         );

INSERT INTO langdivision (
                             id,
                             name,
                             langarea_id
                         )
                         VALUES (
                             19,
                             '(area Walser)',
                             11
                         );

-- Table: sample
INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       1,
                       'trm_itdia_bo_m1_bologna.mp3',
                       NULL,
                       1,
                       5,
                       1
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       2,
                       'trm_itdia_mi_f_milano.mp3',
                       NULL,
                       1,
                       1,
                       6
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       3,
                       'trm_itdia_na_f2_napoli.mp3',
                       NULL,
                       1,
                       6,
                       2
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       4,
                       'trm_itdia_pi_m_pisa.mp3',
                       NULL,
                       1,
                       2,
                       5
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       5,
                       'trm_itdia_ta_m_taranto.mp3',
                       NULL,
                       1,
                       3,
                       4
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       6,
                       'trm_itdia_ve_m_cazzago.mp3',
                       NULL,
                       1,
                       4,
                       3
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       7,
                       'vnc_bologna.mp3',
                       NULL,
                       2,
                       5,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       8,
                       'vnc_milano.mp3',
                       NULL,
                       2,
                       1,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       9,
                       'vnc_napoli.mp3',
                       NULL,
                       2,
                       6,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       10,
                       'vnc_pianiga.mp3',
                       NULL,
                       2,
                       8,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       11,
                       'vnc_pisa.mp3',
                       NULL,
                       2,
                       2,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       12,
                       'vnc_taranto.mp3',
                       NULL,
                       2,
                       3,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       14,
                       'trm_itdia_aq_m_l_aquila.mp3',
                       NULL,
                       1,
                       9,
                       7
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       15,
                       'vnc_venere.mp3',
                       NULL,
                       2,
                       16,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       16,
                       'trm_itdia_pz_f_alianello.mp3',
                       NULL,
                       1,
                       10,
                       8
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       17,
                       'trm_arberesh_cs_f_frascineto.mp3',
                       NULL,
                       1,
                       11,
                       9
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       18,
                       'vnc_noicattaro.mp3',
                       NULL,
                       2,
                       12,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       19,
                       'trm_itdia_ud_f_agrons.mp3',
                       NULL,
                       1,
                       13,
                       10
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       20,
                       'vnc_gorizia.mp3',
                       NULL,
                       2,
                       14,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       21,
                       'trm_itdia_lt_f_fondi.mp3',
                       NULL,
                       1,
                       15,
                       11
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       22,
                       'vnc_roma.mp3',
                       NULL,
                       2,
                       17,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       23,
                       'trm_itdia_ge_f_genova.mp3',
                       NULL,
                       1,
                       18,
                       12
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       24,
                       'vnc_genova.mp3',
                       NULL,
                       2,
                       18,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       25,
                       'trm_itdia_an_m_jesi.mp3',
                       NULL,
                       1,
                       19,
                       13
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       26,
                       'vnc_tolentino.mp3',
                       NULL,
                       2,
                       20,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       27,
                       'trm_itdia_is_m_isernia.mp3',
                       NULL,
                       1,
                       21,
                       14
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       28,
                       'vnc_campobasso.mp3',
                       NULL,
                       2,
                       22,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       29,
                       'trm_occitan_cn_f_bellino.mp3',
                       NULL,
                       1,
                       23,
                       15
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       30,
                       'vnc_torino.mp3',
                       NULL,
                       2,
                       24,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       31,
                       'trm_itdia_ca_m_cagliari.mp3',
                       NULL,
                       1,
                       25,
                       16
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       32,
                       'vnc_nuoro.mp3',
                       NULL,
                       2,
                       26,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       33,
                       'trm_itdia_me_f_san_fratello.mp3',
                       NULL,
                       1,
                       27,
                       17
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       34,
                       'vnc_palermo.mp3',
                       NULL,
                       2,
                       28,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       35,
                       'trm_itdia_pg_f_castiglione_del_lago.mp3',
                       NULL,
                       1,
                       29,
                       18
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       36,
                       'vnc_perugia.mp3',
                       NULL,
                       2,
                       30,
                       NULL
                   );

INSERT INTO sample (
                       id,
                       audio,
                       transcript,
                       type_id,
                       city_id,
                       division_id
                   )
                   VALUES (
                       37,
                       'trm_walser_ao_m2_issime.mp3',
                       NULL,
                       1,
                       31,
                       19
                   );

COMMIT TRANSACTION;
