"""This module contains the steps to implement the PostgreSQL database.
To create the database, move to the current directory and run the module:
$ python3 postgres.py"""


import psycopg2
from psycopg2 import OperationalError


# Connect to the default postgres database
def create_connection(db_name, db_user, db_password, db_host, db_port):
    """Create a connection to the default postgres database."""

    connection = None
    try:
        connection = psycopg2.connect(
            database=db_name,
            user=db_user,
            password=db_password,
            host=db_host,
            port=db_port
        )
    except OperationalError as e:
        print(f'The following error occurred: {e}')

    return connection


connection = create_connection(
    'postgres', 'developer', 'dev', 'localhost', '5432'
)


# Create the database for the project
def create_database(connection, query):
    """Create the development database."""

    connection.autocommit = True
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        print('Database created successfully')
    except OperationalError as e:
        print(f'The error {e} occurred')


create_database_query = 'CREATE DATABASE diatlas'
create_database(connection, create_database_query)
