"""empty message

Revision ID: 69386fc9181a
Revises: a3d8abe29805
Create Date: 2020-06-02 11:39:04.207247

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '69386fc9181a'
down_revision = 'a3d8abe29805'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('province',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=25), nullable=False),
    sa.Column('region_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['region_id'], ['region.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.add_column('city', sa.Column('province_id', sa.Integer(), nullable=False, server_default='0'))
    op.drop_constraint('city_region_id_fkey', 'city', type_='foreignkey')
    op.create_foreign_key(None, 'city', 'province', ['province_id'], ['id'])
    op.drop_column('city', 'region_id')
    op.drop_column('city', 'province')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('city', sa.Column('province', sa.VARCHAR(length=25), server_default=sa.text("'Non disponibile.'::character varying"), autoincrement=False, nullable=False))
    op.add_column('city', sa.Column('region_id', sa.INTEGER(), autoincrement=False, nullable=False))
    op.drop_constraint(None, 'city', type_='foreignkey')
    op.create_foreign_key('city_region_id_fkey', 'city', 'region', ['region_id'], ['id'])
    op.drop_column('city', 'province_id')
    op.drop_table('province')
    # ### end Alembic commands ###
