"""empty message

Revision ID: 7175b1088040
Revises: d486699d05d4
Create Date: 2020-06-03 10:18:27.654326

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7175b1088040'
down_revision = 'd486699d05d4'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('city', 'province_id',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.alter_column('langdivision', 'langarea_id',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.alter_column('region', 'capital',
               existing_type=sa.VARCHAR(length=25),
               nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('region', 'capital',
               existing_type=sa.VARCHAR(length=25),
               nullable=True)
    op.alter_column('langdivision', 'langarea_id',
               existing_type=sa.INTEGER(),
               nullable=True)
    op.alter_column('city', 'province_id',
               existing_type=sa.INTEGER(),
               nullable=True)
    # ### end Alembic commands ###
