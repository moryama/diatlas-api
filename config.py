import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    """Basic configuration"""
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevConfig(Config):
    FLASK_ENV = 'development'
    DEBUG = True
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    # SQLALCHEMY_ECHO = True


class TestConfig(Config):
    FLASK_ENV = 'test'
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///test.db'
    SERVER_NAME = '127.0.0.1:5000'  # To access app_context() in tests
    SECRET_KEY = 'test'
