[![pipeline status](https://gitlab.com/moryama/diatlas-api/badges/master/pipeline.svg)](https://gitlab.com/moryama/diatlas-api/-/commits/master)

# Diatlas API (_ongoing project_)

## Welcome!

If you have any questions or comments about this project, I'd love to hear them!

In case you don't have my personal email, just use this: 5599992-moryama@users.noreply.gitlab.com

(it says noreply but I _will_ reply).

## What is this

This is a RESTful API I'm building for Diatlas, a quiz game for Italian language lovers.

The API main use is to serve questions and check answers to be consumed by the client to create interactive quizzes.

As a nice plus, it will also serve to access a database of linguistic samples from Italian regional voices and dialects.

A link to the API documentation will be added soon.

# DEVELOPMENT NOTES

# Building Tools

**Framework**  :  Flask

**ORM**        :  SQLAlchemy

**Database**   :  PostgreSQL

**API**        :  Connexion

**Tests**      :  Pytest


# Installation and Usage

Clone the project.

Create your virtual environment (recommended).

Install the dependencies.

Set any environment variables (check the [section below](#Environment-variables)).

Run the server:

`$ flask run`

To run the tests:

`$ pytest`


# API with Connexion

To manage the API and its Swagger documentation, we make use of the light [Connexion](https://connexion.readthedocs.io) Flask framework.

It comes with a Swagger UI Console for interactive documentation.

Just run the server `$ flask run` then visit [https://localhost:5000/game/ui](https://localhost:5000/game/ui) to check it out.

## How Connexion works

What Connexion does is basically acting as an external layer to the Flask application. This way, it lets us manage the API endpoints and documentation while maintaining the Flask app properties.

This is where Connexion takes action:

- a file `swagger.yaml` is created. This is where the API structure is described. This file needs to live in a folder in the root path of the application.

- inside the application factory in `__init__.py`. Following the official documentation a Connexion FlaskApp is created here, referencing to the YAML file. I then decided to isolate the Flask app to access properties like the configuration. By the way, if you are wondering, the `config.py` file has to live outside our `app/` directory for Flask to find it.

# Environment variables

The project uses [python-dotenv](https://pypi.org/project/python-dotenv/) to manage environment variables, so that we don't need to reload them every time.

## Installation and usage

This should take a minute.

Install the python-dotenv package

`$ pip install python-dotenv`

Then create a `.flaskenv` file to store Flask environment variables such as `FLASK_APP=app`

and `FLASK_ENV=development`.

Also create a `.env` file to store the app's environment variables such as `SECRET_KEY=super-secret` and `SQLALCHEMY_DATABASE_URI=postgresql+psycopg2://developer:dev@localhost/diatlas`

Run as usual.

**REMINDER**: Don't forget to add the `.flaskenv` and **especially** the `.env` to your `.gitignore` file.
