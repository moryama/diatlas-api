"""This module defines the API endpoints."""


from flask import Blueprint, Response


bp = Blueprint('home', __name__,
               static_folder='static')


@bp.route('/', methods=['GET', 'POST'])
def home():
    """Route to the homepage."""

    return Response('All is good.', 200)
