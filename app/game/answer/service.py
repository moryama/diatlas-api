import sys

from app import db
from app.data.models import Sample


def get_sample_by_id(sample_id):

    sample = db.session.query(Sample)\
                             .filter(Sample.id == sample_id)\
                             .first()
    return sample


def get_right_answer(question_id):

    sample_id = question_id
    sample = get_sample_by_id(sample_id)

    if sample is not None:
        right_answer = sample.city_province_region()
        return right_answer
    else:
        sys.exit('Could not find a sample with this id in the database.')
