"""This module defines the API endpoints."""


from flask import Blueprint, jsonify

from app.game.answer.model import Answer
from app.game.errors import request_error
from app.game.validation import (arguments_type_is_valid,
                                 question_id_is_valid)

from .service import get_right_answer

bp = Blueprint('feedback', __name__,
               static_folder='static')


@bp.route('/game/answer/<int:question_id>/<string:user_answer>', methods=['POST'])
def post_answer(question_id: int, user_answer: str):
    """Posts an answer to a question
    and returns a feedback object in json format.
    """

    if not arguments_type_is_valid(post_answer,
                                   question_id=question_id,
                                   user_answer=user_answer):
        return jsonify(request_error(code=400,
                                     message='Argument type is not valid')), 400

    if not question_id_is_valid(question_id):
        return jsonify(request_error(code=400,
                                     message='This <question_id> is not valid')), 400

    right_answer = get_right_answer(question_id)

    feedback = Answer(right_answer).give_feedback(user_answer)

    return jsonify(feedback)
