class Answer():

    def __init__(self, right_answer):

        if right_answer is None:
            raise ValueError('You need to pass the right answer to get your feedback')

        self.right_answer = right_answer

    def give_feedback(self, user_answer):

        if user_answer == '' or user_answer is None:
            raise ValueError('Cannot take an empty string for an answer')

        if user_answer == self.right_answer:
            return {'data': {'isCorrect': True,
                             'userAnswer': user_answer}
                    }
        else:
            return {'data': {'isCorrect': False,
                             'userAnswer': user_answer,
                             'rightAnswer': self.right_answer}
                    }
