import random
from typing import Dict, List

from app.data.models import Sample
from app.game.exceptions import SingleInputError
from app.game.question.service import (
    get_all_area_ids, get_all_region_ids,
    get_area_ids_and_sample_ids_by_type_and_area_id,
    get_region_ids_and_sample_ids_by_type_and_region_id, get_samples_by_id)
from app.game.question.utils import (
    convert_list_of_tuples_to_dictionary_organized_by_common_first_element,
    get_one_random_item_per_group, convert_list_of_tuples_to_list_of_int)


# ======================== BY AREA ========================


def get_n_random_area_ids(n: int) -> List[int]:

    area_ids = get_all_area_ids()

    n_random_area_ids = random.sample(area_ids, n)

    return convert_list_of_tuples_to_list_of_int(n_random_area_ids)


def collect_sample_ids_by_area(sample_type: str, area_ids: List[int]) -> Dict[int, List[int]]:

    area_ids_and_sample_ids = get_area_ids_and_sample_ids_by_type_and_area_id(
        sample_type,
        area_ids
        )

    sample_ids_by_area = convert_list_of_tuples_to_dictionary_organized_by_common_first_element(area_ids_and_sample_ids)

    return sample_ids_by_area


def pick_n_samples_from_n_different_areas(sample_type: str, n: int) -> List[Sample]:

    random_area_ids = get_n_random_area_ids(n)

    sample_ids_by_area = collect_sample_ids_by_area(sample_type, random_area_ids)

    random_ids = get_one_random_item_per_group(sample_ids_by_area)

    samples = get_samples_by_id(random_ids)

    return samples


# ======================== BY REGION ========================


def get_n_random_region_ids(n: int) -> List[int]:

    region_ids = get_all_region_ids()

    n_random_region_ids = random.sample(region_ids, n)

    return convert_list_of_tuples_to_list_of_int(n_random_region_ids)


def collect_sample_ids_by_region(sample_type: str, region_ids: List[int]) -> Dict[int, List[int]]:

    region_ids_and_sample_ids = get_region_ids_and_sample_ids_by_type_and_region_id(
        sample_type,
        region_ids
        )

    sample_ids_by_region = convert_list_of_tuples_to_dictionary_organized_by_common_first_element(region_ids_and_sample_ids)  # noqa

    return sample_ids_by_region


def pick_n_samples_from_n_different_regions(sample_type: str, n: int) -> List[Sample]:

    random_region_ids = get_n_random_region_ids(n)

    sample_ids_by_region = collect_sample_ids_by_region(sample_type, random_region_ids)

    random_ids = get_one_random_item_per_group(sample_ids_by_region)

    samples = get_samples_by_id(random_ids)

    return samples


def make_multiple_choice(choices):
    """Returns the elements to compose a multiple choice type of question.

    Additionally, it returns the <question_id> which equals to the id of the
    sample that makes the right answer. This value is passed in order to be
    used as identifier to check the right answer.

    :param choices: a list of Sample objects
    For example: [<Sample 2>, <Sample 12>, <Sample 14>]

    :return: a list of values
    For example:    [2,
                    'vnc_venere.mp3', [
                    'Venere, AQ, Abruzzo',
                    'Alianello, PZ, Basilicata',
                    'Gorizia, GO, Friuli-Venezia-Giulia'
                    ]]
    """

    if len(choices) == 1:
        raise SingleInputError('Input of length 1 is not allowed.')

    target = random.choice(choices)
    question_id = target.id
    stem = target.audio

    options = []
    for choice in choices:
        options.append(choice.city_province_region())
    random.shuffle(options)

    multiple_choice = [question_id, stem, options]

    return multiple_choice
