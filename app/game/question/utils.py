import random
from typing import Dict, List, Tuple


def convert_list_of_tuples_to_list_of_int(arr: List[Tuple[int]]) -> List[int]:
    """Convert a list of single value tuples into a list of integers.

    :param array: [(1,), (2,), (3,)]
    :return: [1, 2, 3]

    """

    return [t for (t,) in arr]


def convert_list_of_tuples_to_dictionary_organized_by_common_first_element(list_of_tuples: List[Tuple[int, ...]]) -> Dict[int, List[int]]:  ## noqa

    """Take a list of tuples where each tuple is a pair of two values.

    Convert into a dictionary organized by the common first elements in the tuples.
    - key is the first element of a tuple
    - value is an array containing the second element of a tuple

    As expected with dictionaries, keys are unique.

    :param array: [(1, 12), (1, 24), (2, 7), (2, 33), (1, 15)]
    :return: {1: [12, 24, 15], 2: [7, 33]}

    """

    output = {}
    for pair in list_of_tuples:

        if len(pair) != 2:
            raise ValueError('Expected exactly 2 elements in the tuple.')

        for item in pair:
            if not isinstance(item, int):
                print(item, 'NOT AN INT!')
                raise ValueError('Expected integer values.')

        key, value = pair[0], pair[1]

        if output.get(key) and value not in output[key]:
            output[key].append(value)
        else:
            output[key] = [value]

    return output


def get_one_random_item_per_group(items_by_group: Dict[int, List[int]]) -> List[int]:

    random_items = []
    for sample_ids_list in items_by_group.values():
        random_items.append(random.choice(sample_ids_list))

    return random_items
