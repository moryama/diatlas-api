from app.game.question.factory import make_multiple_choice


class Question():

    def __init__(self, sample_type, level, samples):
        self.sample_type = sample_type
        self.level = level
        self.samples = samples

    def make(self):

        question_id, stem, options = make_multiple_choice(self.samples)

        question_data = {'question': {
                         'id': question_id,
                         'audio': stem
                         },
                         'options': options}

        return {'data': question_data}
