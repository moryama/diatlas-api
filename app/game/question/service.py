# type: ignore
from typing import List, Tuple

from app import db
from app.data.models import Area, City, Province, Region, Sample, Type


def get_samples_by_id(sample_ids: List[int]) -> List[Sample]:

    samples = []
    for sample_id in sample_ids:
        sample = db.session.query(Sample)\
            .filter(Sample.id == sample_id)\
            .first()
        samples.append(sample)
    return samples


def get_all_area_ids():
    return db.session.query(Area.id)\
        .all()


def get_all_region_ids():
    return db.session.query(Region.id)\
        .all()


def get_area_ids_and_sample_ids_by_type_and_area_id(sample_type: str, area_ids: List[int]) -> List[Tuple[int]]:
    return db.session.query(Area.id, Sample.id)\
        .join(City, City.id == Sample.city_id)\
        .join(Province, Province.id == City.province_id)\
        .join(Region, Region.id == Province.region_id)\
        .join(Area, Area.id == Region.area_id)\
        .join(Type, Type.id == Sample.type_id)\
        .filter(Area.id.in_(area_ids))\
        .filter(Type.name == sample_type)\
        .all()


def get_region_ids_and_sample_ids_by_type_and_region_id(sample_type: str, region_ids: List[int]) -> List[Tuple[int]]:
    return db.session.query(Region.id, Sample.id)\
        .join(City, City.id == Sample.city_id)\
        .join(Province, Province.id == City.province_id)\
        .join(Region, Region.id == Province.region_id)\
        .join(Type, Type.id == Sample.type_id)\
        .filter(Region.id.in_(region_ids))\
        .filter(Type.name == sample_type)\
        .all()
