"""This module contains the definitions of game levels"""

from typing import List

from app.data.models import Sample
from app.game.question.factory import (
    pick_n_samples_from_n_different_areas,
    pick_n_samples_from_n_different_regions)


GAME_LEVELS = {
    'easy': {
        'get_choices': pick_n_samples_from_n_different_areas,
        'choices_num': 2
    },
    'medium': {
        'get_choices': pick_n_samples_from_n_different_areas,
        'choices_num': 3
    },
    'hard': {
        'get_choices': pick_n_samples_from_n_different_regions,
        'choices_num': 4
    }
}


def get_question_choices_by_type_and_level(sample_type: str, game_level: str) -> List[Sample]:

    get_choices = GAME_LEVELS[game_level]['get_choices']
    number_of_choices = GAME_LEVELS[game_level]['choices_num']

    choices = get_choices(
        sample_type,
        number_of_choices
        )

    return choices
