"""This module defines the question endpoint."""

from flask import Blueprint, jsonify

from app.game.errors import request_error
from app.game.question.levels import get_question_choices_by_type_and_level
from app.game.validation import (arguments_type_is_valid, level_is_valid,
                                 sample_type_is_valid)

from .model import Question

bp = Blueprint('question', __name__,
               static_folder='static')


@bp.route('/game/question/<string:sample_type>/<string:level>')
def get_question(sample_type: str, level: str):
    """Returns a question object in json format."""

    if not arguments_type_is_valid(get_question,
                                   sample_type=sample_type,
                                   level=level):
        return jsonify(request_error(code=400,
                                     message='Argument type is not valid')), 400

    if not level_is_valid(level):
        return jsonify(request_error(code=400,
                                     message='This <level> is not valid')), 400

    if not sample_type_is_valid(sample_type):
        return jsonify(request_error(code=400,
                                     message='This <sample_type> is not valid')), 400

    choices = get_question_choices_by_type_and_level(sample_type, level)

    question = Question(sample_type, level, choices)
    question_data = question.make()

    return jsonify(question_data)
