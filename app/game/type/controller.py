from flask import Blueprint, jsonify
from app.game.type.service import GameTypeService


bp = Blueprint('types', __name__)


@bp.route('/game/types')
def get_all():

    all_types = GameTypeService.get_all()

    game_types = []
    for t in all_types:
        game_types.append(dict(name=t.name,
                               label=t.label,
                               label_plural=t.label_plural))

    game_types_data = dict(gameTypes=game_types)

    return jsonify(dict(data=game_types_data))
