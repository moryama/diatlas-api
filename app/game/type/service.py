from app.data.models import Type


class GameTypeService():

    def get_all():

        all_types = Type.query.all()
        return all_types
