"""Exceptions used within the application."""


# TODO: Make as a regular ValueError?
class SingleInputError(Exception):
    """Gets raised when an argument of lentgh 1 is passed."""
