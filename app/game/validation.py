from typing import get_type_hints

from app import db
from app.data.models import Sample, Type
from app.game.question.levels import GAME_LEVELS


MIN_ITEMS_IN_QUESTION = 2


def arguments_type_is_valid(object, **kwargs):
    hints = get_type_hints(object)

    for param_name, param_type in hints.items():

        # We are only checking for input values
        if param_name == 'return':
            continue

        try:
            param = kwargs[param_name]
        except KeyError as e:
            print(f'KeyError: the parameter {e} was not passed')
            raise

        if not isinstance(param, param_type):
            return False

    return True


def get_number_of_samples_of_given_type(sample_type):
    number_of_samples_of_given_type = db.session.query(Sample.id)\
                                      .join(Type, Type.id == Sample.type_id)\
                                      .filter(Type.name == sample_type)\
                                      .count()

    return number_of_samples_of_given_type


def sample_type_is_valid(sample_type):

    if sample_type == '' or sample_type is None:
        return False

    number_of_samples_of_given_type = get_number_of_samples_of_given_type(sample_type)

    if number_of_samples_of_given_type < MIN_ITEMS_IN_QUESTION:
        return False

    return True


def level_is_valid(level):

    level = GAME_LEVELS.get(level, None)
    return level is not None


def question_id_exists(question_id):

    # https://stackoverflow.com/questions/32938475/flask-sqlalchemy-check-if-row-exists-in-table
    question_id_exists = db.session.query(
                           db.session.query(Sample.id)
                           .filter(Sample.id == question_id).exists())\
                           .scalar()

    return question_id_exists


def question_id_is_valid(question_id):

    if question_id <= 0:
        return False

    if not question_id_exists(question_id):
        return False

    return True
