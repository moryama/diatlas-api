"""Exceptions used within the the game API endpoints."""


def request_error(code, message):
    error_data = {'success': False,
                  'error': {'code': code,
                            'message': message}
                  }
    return error_data
