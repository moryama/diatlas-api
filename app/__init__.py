"""The application is created here."""

import os

import connexion
from flask_sqlalchemy import SQLAlchemy
from werkzeug.exceptions import NotFound
from flask_migrate import Migrate


# Global plugins
db = SQLAlchemy()


def create_app(testing=False):
    """Create the app, set any configuration."""

    connex_app = connexion.FlaskApp(__name__, specification_dir='./')
    connex_app.add_api('swagger.yaml')

    app = connex_app.app  # Isolate Flask app from Connexion FlaskApp

    # Set configuration
    flask_env = os.getenv('FLASK_ENV', None)

    if testing or flask_env == 'test':
        app.config.from_object('config.TestConfig')
    elif flask_env == 'development':
        app.config.from_object('config.DevConfig')
    else:
        app.config.from_object('config.Config')

    with app.app_context():
        # Initialize plugins
        db.init_app(app)
        assert db, 'Database was not initialized.'

        migrate = Migrate(app, db)  # noqa

        # Create tables if they don't exist yet
        from .data.models import (Area,  # noqa
                                  City,
                                  Langarea,
                                  Langdivision,
                                  Region,
                                  Sample,
                                  Type)
        db.create_all()
        assert len(db.metadata.tables) > 0, 'No table was created.'

    # Error handler
    @app.errorhandler(NotFound)
    def handle_bad_request(e):
        return '404 - Non ho trovato la pagina che cercavi', 404

    # TODO: Refactor
    # Register Blueprints
    from . import home
    app.register_blueprint(home.bp)
    from .game.question import controller as question
    app.register_blueprint(question.bp)
    from .game.answer import controller as answer
    app.register_blueprint(answer.bp)
    from .game.type import controller as type
    app.register_blueprint(type.bp)

    return app
