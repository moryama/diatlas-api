swagger: "2.0"
info:
  description: This is the swagger file that goes with our server code
  version: "1.0.0"
  title: Diatlas Game API
consumes:
  - application/json
produces:
  - application/json

basePath: "/game"

paths:
  /question/{sample_type}/{level}:
    get:
      operationId: app.game.question.controller.get_question
      tags:
        - Question
      summary: Get a random multiple choice question
      description: Get a random multiple choice question of given type and level
      parameters:
        - name: sample_type
          in: path
          description: Type of sample to play with
          required: True
          type: string
        - name: level
          in: path
          description: Game level to play with
          required: True
          type: string
      responses:
        200:
          description: Successfully got random question
          schema:
            type: object
            required:
              - question
              - options
            properties:
              question:
                description: A question item
                properties:
                  id:
                    description: The question id, which can be used via POST/answer to retrieve the correct answer
                    type: integer
                  audio:
                    description: Path of the audio file to be played
                    type: string
              options:
                description: List of options to make a multiple choice question
                type: array
                items:
                  description: An option that is a potential answer to the question
                  type: string
        400:
          description: An argument of invalid type was supplied
          schema:
            type: object
            properties:
              success:
                type: boolean
              error:
                type: object
                properties:
                  code:
                    type: integer
                  message:
                    type: string
        400:
          description: An invalid sample type was supplied
          schema:
            type: object
            properties:
              success:
                type: boolean
              error:
                type: object
                properties:
                  code:
                    type: integer
                  message:
                    type: string
        400:
          description: An invalid level was supplied
          schema:
            type: object
            properties:
              success:
                type: boolean
              error:
                type: object
                properties:
                  code:
                    type: integer
                  message:
                    type: string

  /answer/{question_id}/{user_answer}:
    post:
      operationId: app.game.answer.controller.post_answer
      tags:
        - Answer
      summary: Send an answer to a question
      description: Send an answer to a question
      parameters:
        - name: question_id
          in: path
          description: The current question id
          required: True
          type: integer
        - name: user_answer
          in: path
          description: Option selected by the user as the correct answer
          required: True
          type: string
      responses:
        200:
          description: Feedback to a user answer
          schema:
            type: object
            required:
              - isCorrect
              - userAnswer
            properties:
              isCorrect:
                type: boolean
              userAnswer:
                type: string
              rightAnswer:
                type: string
        400:
          description: An argument of invalid type was supplied
          schema:
            type: object
            properties:
              success:
                type: boolean
              error:
                type: object
                properties:
                  code:
                    type: integer
                  message:
                    type: string
        400:
          description: An invalid question id was supplied
          schema:
            type: object
            properties:
              success:
                type: boolean
              error:
                type: object
                properties:
                  code:
                    type: integer
                  message:
                    type: string

  /types:
    get:
      operationId: app.game.type.controller.get_all
      summary: Get all types
      description: Get all sample types available for the game, each with its labels
      responses:
        200:
          description: Successfully got all types
          schema:
            type: object
            required:
              - gameTypes
            properties:
              gameTypes:
                description: List of types available for the game
                type: array
                items:
                  required:
                    - name
                    - label
                    - label_plural
                  properties:
                    name:
                        description: The type name
                        type: string
                    label:
                        description: The type label in a singular form
                        type: string
                    label_plural:
                        description: The type label in a plural form
                        type: string
