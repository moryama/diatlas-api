import pytest

from app.game.answer.model import Answer


@pytest.fixture
def answer():
    return Answer(right_answer='right_answer')


@pytest.fixture
def positive_feedback_data():
    return {'data': {'isCorrect': True,
                     'userAnswer': 'right_answer'}
            }


@pytest.fixture
def negative_feedback_data():
    return {'data': {'isCorrect': False,
                     'userAnswer': 'some_wrong_answer',
                     'rightAnswer': 'right_answer'}
            }


class TestFeedbackClass():

    def test_create_feedback_object(self, answer):
        feedback = answer

        assert feedback
        assert feedback.right_answer == 'right_answer'

    def test_give_positive_feedback(self,
                                    app,
                                    answer,
                                    positive_feedback_data):
        user_answer = 'right_answer'

        with app.app_context():
            assert answer.give_feedback(user_answer) == positive_feedback_data

    def test_give_negative_feedback(self,
                                    app,
                                    answer,
                                    negative_feedback_data):
        user_answer = 'some_wrong_answer'

        with app.app_context():
            assert answer.give_feedback(user_answer) == negative_feedback_data


class TestFeedbackExceptions():

    def test_fails_if_right_answer_is_None(self):
        with pytest.raises(ValueError):
            Answer(right_answer=None)

    def test_fails_if_user_answer_is_empty(self, answer, app):
        with pytest.raises(ValueError):
            with app.app_context():
                assert answer.give_feedback(user_answer='')

    def test_fails_if_user_answer_is_none(self, answer, app):
        with pytest.raises(ValueError):
            with app.app_context():
                assert answer.give_feedback(user_answer=None)
