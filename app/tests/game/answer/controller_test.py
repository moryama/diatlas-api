from json.decoder import JSONDecodeError

import pytest

from app.tests.conftest import (
    assert_status_code_is_ok_and_content_type_is_json, get_json_data)


class TestPostAnswer():

    def test_feedback_for_right_answer_is_returned(self, client):
        question_id = 1
        user_answer = 'city1, province1, region1'
        right_answer = 'city1, province1, region1'
        response, data = get_json_data(
                            client,
                            f'/game/answer/{question_id}/{user_answer}',
                            'post')
        assert_status_code_is_ok_and_content_type_is_json(response)
        assert data['data']['isCorrect'] is True
        assert data['data']['userAnswer'] == right_answer

    def test_feedback_for_wrong_answer_is_returned(self, client):
        question_id = 1
        user_answer = 'incorrect_answer'
        right_answer = 'city1, province1, region1'
        response, data = get_json_data(
                            client,
                            f'/game/answer/{question_id}/{user_answer}',
                            'post')
        assert_status_code_is_ok_and_content_type_is_json(response)
        assert data['data']['isCorrect'] is False
        assert data['data']['userAnswer'] == user_answer
        assert data['data']['rightAnswer'] == right_answer

    @pytest.mark.parametrize((
        'question_id', 'user_answer'), (
            ('', ''),
            ('', 1),
            ([], ''),
            ((), '')
        ))
    def test_path_variables_of_wrong_type(self, client,
                                          question_id,
                                          user_answer):
        url = f'/game/answer/{question_id}/{user_answer}'

        with pytest.raises(JSONDecodeError):
            response, data = get_json_data(client, url, 'post')

            assert data['success'] is False
            assert response.status_code == 400
            assert data['error']['message'] == 'Argument type is not valid'

    @pytest.mark.parametrize((
        'question_id', 'user_answer', 'error_code', 'error_message'), (
            ('one', 'city1, province1, region1', 400, 'This <question_id> is not valid'),
            (None, 'city1, province1, region1', 400, 'This <question_id> is not valid'),
            ('', 'city1, province1, region1', 400, 'This <question_id> is not valid'),
            (1, '', 400, 'No valid <user_answer>')
        ))
    def test_catches_exception_for_invalid_path_variables(self, client,
                                                          question_id,
                                                          user_answer,
                                                          error_code,
                                                          error_message):
        with pytest.raises(JSONDecodeError):
            url = f'/game/answer/{question_id}/{user_answer}'

            response, data = get_json_data(client, url, 'posts')

            assert data['success'] is False
            assert response.status_code == error_code
            assert data['error']['message'] == error_message
