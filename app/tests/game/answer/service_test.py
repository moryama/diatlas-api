import pytest

from app.data.models import Sample
from app.game.answer.service import get_right_answer
from app.game.answer.service import get_sample_by_id


class TestGetRightAnswer():

    def test_return_right_answer(self, app):
        question_id = 1
        known_right_answer = 'city1, province1, region1'
        with app.app_context():
            retrieved_right_answer = get_right_answer(question_id)
        assert retrieved_right_answer == known_right_answer

    def test_returns_none_if_no_sample_is_found_in_database(self, app):
        question_id = 0
        with pytest.raises(SystemExit):
            answer = get_right_answer(question_id)
            assert answer is None


class TestGetSampleById():

    def test_return_sample_object(self, app):
        sample_id = 1
        with app.app_context():
            sample = get_sample_by_id(sample_id)
        assert isinstance(sample, Sample)
