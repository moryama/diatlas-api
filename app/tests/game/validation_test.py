import pytest

from app.game.answer.controller import post_answer
from app.game.validation import (arguments_type_is_valid, level_is_valid,
                                 question_id_exists, question_id_is_valid,
                                 sample_type_is_valid)
from app.tests import mock_data


class TestArgumentsTypeIsValid():

    def test_should_validate_known_values(self):
        assert arguments_type_is_valid(object=post_answer,
                                       question_id=1,
                                       user_answer='user answer')

    def test_should_return_true_if_empty_string_is_passed(self):
        assert arguments_type_is_valid(object=post_answer,
                                       question_id=1,
                                       user_answer='')

    def test_should_return_false_when_string_is_passed_instead_of_integer(self):
        assert not arguments_type_is_valid(object=post_answer,
                                           question_id='',
                                           user_answer=1)

    def test_should_return_false_when_list_is_passed_instead_of_integer(self):
        assert not arguments_type_is_valid(object=post_answer,
                                           question_id=[],
                                           user_answer=1)

    def test_should_return_false_when_dictionary_is_passed_instead_of_integer(self):
        assert not arguments_type_is_valid(object=post_answer,
                                           question_id={},
                                           user_answer=1)

    def test_should_return_false_when_float_is_passed_instead_of_integer(self):
        assert not arguments_type_is_valid(object=post_answer,
                                           question_id=1.5,
                                           user_answer='user answer')

    def test_should_return_false_when_integer_is_passed_instead_of_string(self):
        assert not arguments_type_is_valid(object=post_answer,
                                           question_id=1,
                                           user_answer=1)

    def test_should_return_false_when_boolean_is_passed_instead_of_string(self):
        assert not arguments_type_is_valid(object=post_answer,
                                           question_id=1,
                                           user_answer=True)

    def test_should_raise_exception_and_return_error_message_when_expected_argument_is_not_passed(self):

        first_parameter_of_two = 1
        missing_parameter = 'user_answer'

        with pytest.raises(KeyError)as e:
            arguments_type_is_valid(object=post_answer,
                                    question_id=first_parameter_of_two)

        assert missing_parameter in str(e.value)


# TODO: parametrize improve formatting
class TestSampleTypepathArgument():

    @pytest.mark.parametrize(('sample_type'), (
                             ('typeA'),
                             ('typeB')))
    def test_should_validate_known_values(self, app, sample_type):
        with app.app_context():
            assert sample_type_is_valid(sample_type)

    @pytest.mark.parametrize(('sample_type'), (
                             ('typeC'),
                             ('typeD'),
                             ('invalid sample type')))
    def test_should_return_false_if_sample_type_is_not_valid(self, app,
                                                             sample_type):
        with app.app_context():
            assert sample_type_is_valid(sample_type) is False


class TestLevelpathArgument():

    @pytest.mark.parametrize(('level'), (
                             ('easy'),
                             ('medium'),
                             ('hard')))
    def test_should_validate_known_values(self, level):
        assert level_is_valid(level)

    @pytest.mark.parametrize(('level'), (
                             ('invalid level'),
                             (''),
                             (None)))
    def test_should_return_false_if_level_is_not_valid(self, level):
        assert level_is_valid(level) is False


class TestQuestionIdPathArgument():

    @pytest.mark.parametrize(('question_id'), (
                             (1),
                             (13)))
    def test_should_return_true_if_question_exists_in_database(self, app, question_id):
        with app.app_context():
            assert question_id_exists(question_id)

    def test_should_return_false_if_question_not_exist_in_database(
                                                        self, app):

        non_existing_question_id = len(mock_data.samples) + 1
        with app.app_context():
            assert question_id_exists(non_existing_question_id) is False

    def test_should_validate_known_value(self, app):
        with app.app_context():
            assert question_id_is_valid(7)

    @pytest.mark.parametrize(('question_id'), (
                             (-3),
                             (0),
                             (1034)))
    def test_should_return_false_if_question_id_is_not_valid(self, app, question_id):
        with app.app_context():
            assert question_id_is_valid(question_id) is False
