from app.tests.conftest import (
    assert_status_code_is_ok_and_content_type_is_json, get_json_data)


class TestGetTypes():

    def test_status_code_and_content_type(self, app, client):
        url = '/game/types'

        response, data = get_json_data(client, url, 'get')

        assert_status_code_is_ok_and_content_type_is_json(response)

    def test_should_return_a_list_of_all_available_types_and_their_labels(self, client):
        url = '/game/types'

        response, data = get_json_data(client, url, 'get')
        results = data['data']['gameTypes']

        assert isinstance(results, list)
        assert len(results) == 3
        for r in results:
            assert r['name']
            assert r['label']
            assert r['label_plural']
