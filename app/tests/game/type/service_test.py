from app import db
from app.data.models import Type
from app.game.type.service import GameTypeService


def test_get_all_should_return_all_types(app):
    new_type = Type(name='typeD',
                    label='type D',
                    label_plural='type Ds')
    db.session.add(new_type)
    db.session.commit()

    with app.app_context():
        list_of_all_types = GameTypeService.get_all()

    assert len(list_of_all_types) == 4
