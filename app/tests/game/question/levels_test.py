import pytest

from app.game.question.levels import get_question_choices_by_type_and_level
from app.tests.conftest import IDS_CATEG


class TestGetQuestionChoicesByTypeAndLevel:

    @pytest.mark.parametrize(('sample_type', 'game_level', 'expected_length'), (
        ('typeA', 'easy', 2),
        ('typeA', 'medium', 3),
        ('typeA', 'hard', 4)
    ))
    def test_should_return_expected_number_of_choices(self, app,
                                                      sample_type,
                                                      game_level,
                                                      expected_length):
        choices = get_question_choices_by_type_and_level(sample_type, game_level)
        assert len(choices) == expected_length

    def test_for_level_hard_should_return_choices_from_different_regions(self, app):
        sample_type = 'typeA'
        game_level = 'hard'

        choices = get_question_choices_by_type_and_level(sample_type, game_level)

        regions = ['region1' if sample.id in IDS_CATEG['typeA_region1'] else
                   'region2' if sample.id in IDS_CATEG['typeA_region2'] else
                   'region3' if sample.id in IDS_CATEG['typeA_region3'] else
                   'region4' for sample in choices]

        assert sorted(list(set(regions))) == sorted(regions)
