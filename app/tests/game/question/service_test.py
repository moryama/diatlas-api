from app.game.question.service import (
    get_area_ids_and_sample_ids_by_type_and_area_id, get_samples_by_id)
from app.tests.conftest import IDS_CATEG


def test_get_samples_by_id(app):

    sample_ids = [1, 4]

    with app.app_context():
        samples = get_samples_by_id(sample_ids)

    assert len(samples) == 2
    assert samples[0].audio == 'audio1.mp3'
    assert samples[1].audio == 'audio4.mp3'


def test_get_area_ids_and_sample_ids_by_type_and_area_id(app):

    sample_type = 'typeA'
    area_ids = [1, 2]

    area_and_sample_ids = get_area_ids_and_sample_ids_by_type_and_area_id(sample_type, area_ids)

    # Returns a list of tuples
    assert [isinstance(pair, tuple) for pair in area_and_sample_ids]

    # Area id and sample id match
    for pair in area_and_sample_ids:
        if pair[0] == 1:
            assert pair[1] in IDS_CATEG['typeA_areaA']
        elif pair[0] == 2:
            assert pair[1] in IDS_CATEG['typeA_areaB']
