import pytest

from app.game.exceptions import SingleInputError
from app.game.question.factory import (make_multiple_choice,
                                       pick_n_samples_from_n_different_areas,
                                       pick_n_samples_from_n_different_regions)
from app.tests.conftest import (IDS_CATEG,
                                make_samples)


class TestPickNSamplesFromNDifferentAreas:

    @pytest.mark.parametrize(('sample_type', 'n'), (
                             ('typeA', 2),
                             ('typeA', 3),
                             ))
    def test_should_return_expected_number_of_unique_items(self, app,
                                                           sample_type,
                                                           n):
        with app.app_context():
            samples = pick_n_samples_from_n_different_areas(sample_type, n)

        assert len(samples) == n
        assert len(samples) == len(set(samples))


class TestPickNSamplesFromNDifferentRegions:

    @pytest.mark.parametrize(('sample_type', 'number_of_samples'), (
                             ('typeA', 3),
                             ('typeA', 4),
                             ))
    def test_should_return_expected_number_of_unique_items(self, app,
                                                           sample_type,
                                                           number_of_samples):

        with app.app_context():
            samples = pick_n_samples_from_n_different_regions(
                sample_type,
                number_of_samples
            )

        assert len(samples) == number_of_samples
        assert len(samples) == len(set(samples))

    def test_should_return_samples_from_different_regions(self, app):
        sample_type = 'typeA'
        number_of_samples = 4
        with app.app_context():
            samples = pick_n_samples_from_n_different_regions(
                sample_type,
                number_of_samples
            )

        regions = ['region1' if sample.id in IDS_CATEG['typeA_region1'] else
                   'region2' if sample.id in IDS_CATEG['typeA_region2'] else
                   'region3' if sample.id in IDS_CATEG['typeA_region3'] else
                   'region4' for sample in samples]

        assert sorted(list(set(regions))) == sorted(regions)


class TestMakeMultipleChoiceItem:

    def test_returned_values_are_not_none(self):
        samples = make_samples(2)
        question_id, stem, options = make_multiple_choice(samples)
        assert question_id is not None
        assert stem is not None
        assert options is not None

    def test_returned_values_type(self):
        samples = make_samples(2)
        question_id, stem, options = make_multiple_choice(samples)
        assert isinstance(question_id, int)
        assert isinstance(stem, str)
        assert isinstance(options, list)

    def test_returns_expected_number_of_options(self):
        samples = make_samples(3)
        _, _, options = make_multiple_choice(samples)
        assert len(options) == 3

    def test_output_content(self):
        samples = make_samples(3)
        _, stem, options = make_multiple_choice(samples)
        assert 'sample' in stem and stem.endswith('.mp3')
        assert sorted(options) == ['1_city, 1_province, 1_region',
                                   '2_city, 2_province, 2_region',
                                   '3_city, 3_province, 3_region'
                                   ]
        matches_stem = 0
        for option in options:
            if option.startswith(str(stem[0])):
                matches_stem += 1
        assert matches_stem == 1

    def test_there_is_one_and_only_one_right_answer(self):
        samples = make_samples(3)
        question_id, _, options = make_multiple_choice(samples)
        right_answer = 0
        for option in options:
            if option.startswith(str(question_id)):
                right_answer += 1
        assert right_answer == 1

    def test_an_exception_is_raised_when_a_single_item_input_is_passed(self):
        with pytest.raises(SingleInputError,
                           match='Input of length 1 is not allowed'):
            samples = make_samples(1)
            make_multiple_choice(samples)

    def test_an_exception_is_raised_when_empty_input_is_passed(self):
        with pytest.raises(IndexError,
                           match='Cannot choose from an empty sequence'):
            input = make_samples(0)
            make_multiple_choice(input)
