import pytest

from app.game.question.model import Question
from app.tests.conftest import make_samples


@pytest.fixture
def question():
    return Question(sample_type='typeA',
                    level='medium',
                    samples=make_samples(2))


class TestQuestionClass():

    def test_create_question_object(self, question):
        assert question
        assert question.sample_type == 'typeA'
        assert question.level == 'medium'
        assert len(question.samples) == 2

    def test_make_method_returns_a_dictionary(self, app, question):
        with app.app_context():
            q = question.make()
        assert isinstance(q, dict)

    def test_make_method_returns_expected_items(self, app, question):

        # Since a question is created randomly,
        # instead of predisposing a fixture for the returned data
        # we test the returned elements one by one.

        with app.app_context():
            q = question.make()
        assert q['data']['question']['audio'] is not None
        assert q['data']['question']['id'] is not None
        assert q['data']['options'] is not None
        assert len(q['data']['options']) == 2

    def test_make_method_returns_expected_data(self, app, question):

        with app.app_context():
            q = question.make()
        question_id = q['data']['question']['id']

        assert question_id in [1, 2]
        assert q['data']['question']['audio'] == f'{question_id}_sample.mp3'
        assert sorted(q['data']['options']) == ['1_city, 1_province, 1_region',
                                                '2_city, 2_province, 2_region']
