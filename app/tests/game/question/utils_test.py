import pytest

from app.game.question.utils import (
    convert_list_of_tuples_to_dictionary_organized_by_common_first_element,
    convert_list_of_tuples_to_list_of_int)


class TestMakeListOfInt:

    def test_input_is_empty(self):
        input = []
        output = []
        assert output == convert_list_of_tuples_to_list_of_int(input)

    def test_input_has_one_element(self):
        input = [(1,)]
        output = [1]
        assert output == convert_list_of_tuples_to_list_of_int(input)

    def test_input_has_multiple_elements(self):
        input = [(1,), (2,), (3,)]
        output = [1, 2, 3]
        assert output == convert_list_of_tuples_to_list_of_int(input)

    def test_input_has_duplicates(self):
        input = [(1,), (2,), (2,)]
        output = [1, 2, 2]
        assert output == convert_list_of_tuples_to_list_of_int(input)

    def test_input_contains_a_list(self):
        input = [([1],), ([4],)]
        output = [[1], [4]]
        assert output == convert_list_of_tuples_to_list_of_int(input)  # type: ignore


class TestConvertListOfTuplesToDictionaryByCommonFirstElement:

    def test_input_is_empty(self):
        input = []
        output = {}
        assert output == convert_list_of_tuples_to_dictionary_organized_by_common_first_element(input)

    def test_input_has_one_element(self):
        input = [(1, 2)]
        output = {1: [2]}
        assert output == convert_list_of_tuples_to_dictionary_organized_by_common_first_element(input)

    def test_input_has_one_element_made_of_a_single_integer(self):
        input = [(1,)]
        with pytest.raises(ValueError):
            convert_list_of_tuples_to_dictionary_organized_by_common_first_element(input)

    def test_input_contains_string_instead_of_integer(self):
        input = [('', '')]
        with pytest.raises(ValueError):
            convert_list_of_tuples_to_dictionary_organized_by_common_first_element(input)  # type: ignore

    def test_no_common_first_element_in_input(self):
        input = [(1, 1), (2, 2), (3, 3)]
        expected = {1: [1], 2: [2], 3: [3]}

        output = convert_list_of_tuples_to_dictionary_organized_by_common_first_element(input)

        assert output == expected

    def test_common_first_element_in_input(self):
        list_of_tuples = [(1, 1), (1, 2), (2, 3), (2, 4), (1, 14)]
        expected = {1: [1, 2, 14], 2: [3, 4]}

        output = convert_list_of_tuples_to_dictionary_organized_by_common_first_element(list_of_tuples)

        assert output == expected

    def test_a_duplicate_tuple_gets_ignored(self):
        list_of_tuples = [(1, 1), (1, 1), (2, 3)]
        expected = {1: [1], 2: [3]}

        output = convert_list_of_tuples_to_dictionary_organized_by_common_first_element(list_of_tuples)

        assert output == expected
