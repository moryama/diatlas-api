from json.decoder import JSONDecodeError

import pytest

from app.tests.conftest import (
    assert_status_code_is_ok_and_content_type_is_json, get_json_data)


class TestGetQuestion():

    def test_status_code_and_content_type(self, app, client):
        url = '/game/question/typeA/medium'
        response, data = get_json_data(client, url, 'get')
        assert_status_code_is_ok_and_content_type_is_json(response)

    def test_a_complete_question_is_returned(self, client):
        url = '/game/question/typeA/medium'

        response, data = get_json_data(client, url, 'get')
        audio_file = data['data']['question']['audio']
        question_id = data['data']['question']['id']
        options = data['data']['options']

        assert audio_file
        assert question_id
        assert len(options) == 3

    @pytest.mark.parametrize((
        'sample_type', 'level'), (
            (1, ''),
            ('', 1),
            ([], ''),
            ('', {}),
            ((), '')
        ))
    def test_path_variables_of_wrong_type(self, client,
                                          sample_type,
                                          level):
        url = f'/game/question/{sample_type}/{level}'

        with pytest.raises(JSONDecodeError):
            response, data = get_json_data(client, url, 'get')

            assert data['success'] is False
            assert response.status_code == 400
            assert data['error']['message'] == 'Argument type is not valid'

    @pytest.mark.parametrize((
        'sample_type', 'level', 'error_code', 'error_message'), (
            ('invalid_sample_type', 'easy', 400, 'This <sample_type> is not valid'),
            ('typeC', 'easy', 400, 'This <sample_type> is not valid'),
            ('typeA', 'invalid_level', 400, 'This <level> is not valid'),
            (None, 'easy', 400, 'This <sample_type> is not valid'),
            ('typeA', None, 400, 'This <level> is not valid')
        ))
    def test_for_invalid_path_variables(self, client,
                                        sample_type,
                                        level,
                                        error_code,
                                        error_message):
        url = f'/game/question/{sample_type}/{level}'

        response, data = get_json_data(client, url, 'get')

        assert data['success'] is False
        assert response.status_code == error_code
        assert data['error']['message'] == error_message

    @pytest.mark.parametrize((
        'sample_type', 'level', 'error_code', 'error_message'), (
            ('', 'easy', 400, 'This <sample_type> is not valid'),
            ('typeA', '', 400, 'This <level> is not valid')
        ))
    def test_catches_exception_if_empty_string_is_passed(self,
                                                         client,
                                                         sample_type,
                                                         level,
                                                         error_code,
                                                         error_message):
        with pytest.raises(JSONDecodeError):
            url = f'/game/question/{sample_type}/{level}'

            response, data = get_json_data(client, url, 'get')

            assert data['success'] is False
            assert response.status_code == error_code
            assert data['error']['message'] == error_message
