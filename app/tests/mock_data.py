from app import db
from app.data.models import (Area,
                             City,
                             Langarea,
                             Langdivision,
                             Region,
                             Province,
                             Sample,
                             Type)


types = [{'name': 'typeA', 'label': 'type A', 'label_plural': 'type As'},
         {'name': 'typeB', 'label': 'type B', 'label_plural': 'type Bs'},
         {'name': 'typeC', 'label': 'type C', 'label_plural': 'type Cs'}
         ]
areas = [{'name': 'areaA'},
         {'name': 'areaB'},
         {'name': 'areaC'}
         ]
regions = [{'name': 'region1', 'capital': 'city1', 'area_id': '1'},
           {'name': 'region2', 'capital': 'city2', 'area_id': '2'},
           {'name': 'region3', 'capital': 'city3', 'area_id': '3'},
           {'name': 'region4', 'capital': 'city4', 'area_id': '1'}
           ]
provinces = [{'name': 'province1', 'region_id': '1'},
             {'name': 'province2', 'region_id': '2'},
             {'name': 'province3', 'region_id': '3'},
             {'name': 'province4', 'region_id': '4'},
             ]
cities = [{'name': 'city1', 'province_id': '1'},
          {'name': 'city2', 'province_id': '2'},
          {'name': 'city3', 'province_id': '3'},
          {'name': 'city4', 'province_id': '4'},
          ]
langareas = [{'name': 'langarea1'},
             {'name': 'langarea2'},
             {'name': 'langarea3'}
             ]
langdiv = [{'name': 'langdiv1', 'langarea_id': '1'},
           {'name': 'langdiv2', 'langarea_id': '2'},
           {'name': 'langdiv3', 'langarea_id': '3'},
           ]
samples = [{'audio': 'audio1.mp3',
            'type_id': '1',
            'city_id': '1',
            'division_id': '1'},
           {'audio': 'audio2.mp3',
            'type_id': '1',
            'city_id': '1',
            'division_id': '1'},
           {'audio': 'audio3.mp3',
            'type_id': '1',
            'city_id': '2',
            'division_id': '1'},
           {'audio': 'audio4.mp3',
            'type_id': '1',
            'city_id': '2',
            'division_id': '1'},
           {'audio': 'audio5.mp3',
            'type_id': '1',
            'city_id': '3',
            'division_id': '1'},
           {'audio': 'audio6.mp3',
            'type_id': '1',
            'city_id': '3',
            'division_id': '1'},
           {'audio': 'audio7.mp3',
            'type_id': '2',
            'city_id': '1',
            'division_id': '2'},
           {'audio': 'audio8.mp3',
            'type_id': '2',
            'city_id': '1',
            'division_id': '2'},
           {'audio': 'audio9.mp3',
            'type_id': '2',
            'city_id': '2',
            'division_id': '2'},
           {'audio': 'audio10.mp3',
            'type_id': '2',
            'city_id': '2',
            'division_id': '2'},
           {'audio': 'audio11.mp3',
            'type_id': '2',
            'city_id': '3',
            'division_id': '2'},
           {'audio': 'audio12.mp3',
            'type_id': '2',
            'city_id': '3',
            'division_id': '2'},
           {'audio': 'audio13.mp3',
            'type_id': '3',
            'city_id': '1',
            'division_id': '1'},
           {'audio': 'audio14.mp3',
            'type_id': '1',
            'city_id': '4',
            'division_id': '1'}
           ]


def load_mock_data():

    for t in types:
        new = Type(name=t['name'], label=t['label'], label_plural=t['label_plural'])
        db.session.add(new)

    for a in areas:
        new = Area(name=a['name'])
        db.session.add(new)

    for r in regions:
        new = Region(name=r['name'],
                     capital=r['capital'],
                     area_id=r['area_id'])
        db.session.add(new)

    for p in provinces:
        new = Province(name=p['name'],
                       region_id=p['region_id'])
        db.session.add(new)

    for c in cities:
        new = City(name=c['name'],
                   province_id=c['province_id'])
        db.session.add(new)

    for la in langareas:
        new = Langarea(name=la['name'])
        db.session.add(new)

    for ld in langdiv:
        new = Langdivision(name=ld['name'],
                           langarea_id=ld['langarea_id'])
        db.session.add(new)

    for s in samples:
        new = Sample(audio=s['audio'],
                     type_id=s['type_id'],
                     city_id=s['city_id'],
                     division_id=s['division_id'])
        db.session.add(new)

    db.session.commit()
