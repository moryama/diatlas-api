"""Test configuration."""

import pytest
from flask import json

from app import create_app, db
from app.data import models

from .mock_data import load_mock_data


@pytest.fixture
def app():
    """Generate app configurated for testing."""

    app = create_app(testing=True)

    with app.app_context():

        # Make test database
        db.drop_all()
        db.create_all()
        load_mock_data()

        yield app

        db.session.remove()
        db.drop_all()


@pytest.fixture
def client(app):
    """Return a test client object."""
    return app.test_client()


def assert_status_code_is_ok_and_content_type_is_json(response):
    assert response.status_code == 200
    assert response.content_type == 'application/json'


def get_json_data(client, url, method='get'):

    if method == 'post':
        response = client.post(url)
    else:
        response = client.get(url)
    data = json.loads(response.get_data(as_text=True))
    return response, data


def make_sample(i):

    region = models.Region(name=f'{i}_region')
    province = models.Province(name=f'{i}_province', region=region)
    city = models.City(name=f'{i}_city', province=province)
    sample = models.Sample(id=i, audio=f'{i}_sample.mp3', city=city)

    return sample


def make_samples(n):

    samples = []
    for i in range(1, (n + 1)):
        samples.append(make_sample(i))

    return samples


# TODO change name to SAMPLE_IDS_BY_CATEG
IDS_CATEG = {'typeA': [1, 2, 3, 4, 5, 6, 14],
             'typeB': [7, 8, 9, 10, 11, 12],
             'typeA_areaA': [1, 2, 14],
             'typeA_areaB': [3, 4],
             'typeA_areaC': [5, 6],
             'typeB_areaA': [7, 8],
             'typeB_areaB': [9, 10],
             'typeB_areaC': [11, 12],
             'typeA_region1': [1, 2],
             'typeA_region2': [3, 4],
             'typeA_region3': [5, 6],
             'typeA_region4': [14]
             }
