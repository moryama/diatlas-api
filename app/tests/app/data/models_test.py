from app.tests.conftest import make_samples


def test_sample_representation_in_the_format_city_province_region():
    sample = make_samples(1)[0]
    expected_output = '1_city, 1_province, 1_region'
    assert sample.city_province_region() == expected_output
