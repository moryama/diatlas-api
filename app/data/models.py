"""Classes mapped to database tables."""

from app import db


class Area(db.Model):
    """Schema for Area model"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), unique=True, nullable=False)

    regions = db.relationship('Region', backref='area', lazy=True)

    def __repr__(self):
        return f'{self.name}'


class Region(db.Model):
    """Schema for Region model"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), unique=True, nullable=False)
    info = db.Column(db.Text)
    capital = db.Column(db.String(25), unique=True, nullable=False)
    area_id = db.Column(db.Integer, db.ForeignKey('area.id'), nullable=False)

    provinces = db.relationship('Province', backref='region', lazy=True)

    def __repr__(self):
        return f'{self.name}'


class Province(db.Model):
    """Schema for Province model"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(25), unique=True, nullable=False)
    region_id = db.Column(db.Integer,
                          db.ForeignKey('region.id'),
                          nullable=False)

    cities = db.relationship('City', backref='province', lazy=True)

    def __repr__(self):
        return f'{self.name}'


class City(db.Model):
    """Schema for City model"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), unique=True, nullable=False)
    province_id = db.Column(db.Integer,
                            db.ForeignKey('province.id'),
                            nullable=False)

    samples = db.relationship('Sample', backref='city', lazy=True)

    def __repr__(self):
        return f'{self.name}'


class Langarea(db.Model):
    """Schema for Language Area model"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), unique=True, nullable=False)

    langdivisions = db.relationship('Langdivision',
                                    backref='langarea',
                                    lazy=True)

    def __repr__(self):
        return f'{self.name}'


class Langdivision(db.Model):
    """Schema for Language Division model"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), unique=True, nullable=False)
    langarea_id = db.Column(db.Integer,
                            db.ForeignKey('langarea.id'),
                            nullable=False)

    samples = db.relationship('Sample', backref='langdivision', lazy=True)

    def __repr__(self):
        return f'{self.name}'


class Type(db.Model):
    """Schema for Type model"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(10), unique=True, nullable=False)
    label = db.Column(db.String(20), unique=True)
    label_plural = db.Column(db.String(20), unique=True)

    samples = db.relationship('Sample', backref='type', lazy=True)

    def __repr__(self):
        return f'{self.name}'


class Sample(db.Model):
    """Schema for Sample model"""

    id = db.Column(db.Integer, primary_key=True)
    audio = db.Column(db.String(45), unique=True, nullable=False)
    transcript = db.Column(db.Text)
    type_id = db.Column(db.Integer, db.ForeignKey('type.id'), nullable=False)
    city_id = db.Column(db.Integer, db.ForeignKey('city.id'), nullable=False)
    division_id = db.Column(db.Integer, db.ForeignKey('langdivision.id'))

    def __repr__(self):
        return f'{self.id}) Sample: {self.type.name} from {self.city.name}'

    def city_province_region(self):
        return ', '.join((self.city.name,
                          self.city.province.name,
                          self.city.province.region.name))
